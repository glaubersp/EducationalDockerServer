#!/bin/bash

set -x

docker exec -u www-data nextcloud php occ --no-warnings config:system:set trusted_domains 0 --value="nextcloud"
docker exec -u www-data nextcloud php occ --no-warnings config:system:set trusted_domains 1 --value="nextcloud.test"

docker exec nextcloud rm -fr /var/www/html/apps/onlyoffice
docker cp ./onlyoffice-owncloud nextcloud:/var/www/html/apps/onlyoffice
docker exec nextcloud chown -R www-data:root /var/www/html/apps/onlyoffice
docker exec -u www-data nextcloud php occ --no-warnings app:enable onlyoffice

docker exec -u www-data nextcloud php occ --no-warnings config:system:set onlyoffice DocumentServerUrl --value="http://onlyoffice-document.test/"
docker exec -u www-data nextcloud php occ --no-warnings config:system:set onlyoffice DocumentServerInternalUrl --value="http://onlyoffice-document-server/"
docker exec -u www-data nextcloud php occ --no-warnings config:system:set onlyoffice StorageUrl --value="http://nextcloud/"
